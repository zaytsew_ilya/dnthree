var Game;

(function(window, document, THREE, _){

    var default_options = {
        showAxes: 1,
        controls: { 
            gameStepTime: 5,
            bouncingSpeed: 0.04
        },
        stats: undefined
    };

    Game = function(options) {
        this.frameTime = 0;
        this.cumulatedFrameTime = 0;
        this.lastFrameTime = Date.now();
        this.step = 0;
        this.options = {};

        for(var attr in default_options) {
            if(options.hasOwnProperty(attr)) 
                this.options[attr] = options[attr];
            else
                this.options[attr] = default_options[attr];
        }

        _.bindAll(this, 'onWindowResize', 'onMouseMove', 'render', 'animate');
    };

    Game.prototype.init = function() {
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
		
		this.raycaster = new THREE.Raycaster();
		this.mouse = new THREE.Vector2();
		
        var light = new THREE.AmbientLight( 0xffd324 );
        this.scene.add( light );

        var spotLight = new THREE.SpotLight( 0xffffff );
        spotLight.position.set( -40, 60, -10 );
        spotLight.castShadow = true;
        this.scene.add( spotLight );

        if(this.options.showAxes) {
            var axes = new THREE.AxisHelper( 20 );
            this.scene.add(axes);
        }

        this.camera_controls = new THREE.OrbitControls( this.camera );
        this.camera_controls.damping = 2;
        this.camera_controls.addEventListener( 'change', this.render );

        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setClearColor(0xEEEEEE, 1.0);
        this.renderer.setSize( window.innerWidth, window.innerHeight );
        this.renderer.shadowMapEnabled = true;
		
        document.body.appendChild( this.renderer.domElement );

        this.camera.position.x = -30;
        this.camera.position.y = 40;
        this.camera.position.z = 30;
        this.camera.lookAt(this.scene.position);

        window.addEventListener( 'resize', this.onWindowResize, false );
		window.addEventListener( 'mousemove', this.onMouseMove, false );

        //var gridHelper = new THREE.GridHelper( 500, 10 );
		//gridHelper.position.y = 0.5;
        //this.scene.add( gridHelper );

        this.cube = this.addCube();

		this.drawTileMap();
    };

	Game.prototype.onMouseMove = function ( event ) {
		this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
		this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
	};

    Game.prototype.addCube = function() {
        var cubeGeometry = new THREE.BoxGeometry(10,10,10);
		var texture = THREE.ImageUtils.loadTexture('img/box.jpg');
		var cubeMaterial = new THREE.MeshLambertMaterial({ map: texture, color: 0x28c0ec });
        cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
        cube.position.x = 5;
        cube.position.y = 5;
        cube.position.z = 5;
        cube.receiveShadow = true;
        cube.castShadow = true;
        this.scene.add(cube);

        return cube;
    };
	
	Game.prototype.drawTileMap = function() {
		var floor  = { type: 'floor',  color: 0xcccccc };
		var wall  = { type: 'wall',  color: 0x000000 };
		
		var map_size = 100;
		var tile_size = 10;
		
		this.tiles = [];
		var map = [];
		for(var i = 0; i < map_size * map_size; i++) {
			map.push(Math.floor(Math.random() * 2) ? floor : wall);
		}

		var x0 = y0 = (map_size * tile_size / 2 - tile_size / 2) * -1;
		
		var tileGeometry = new THREE.PlaneGeometry(tile_size,tile_size);
		var floorMaterial = new THREE.MeshBasicMaterial({ color: 0xcccccc, });
		var wallMaterial = new THREE.MeshBasicMaterial({ color: 0x000000, });
		
		var drawTile = function(scene, x, y, tile_size, type) {
			var material = type.type == 'wall' ? wallMaterial : floorMaterial;
			var tile = new THREE.Mesh(tileGeometry, material.clone());
			tile.rotation.x = -0.5 * Math.PI;
			tile.position.x = x;
			tile.position.y = 0;
			tile.position.z = y;
			tile.receiveShadow = true;
			tile.frustumCulled = true;
			tile.name = "Tile";
			tile.initColor = material.color;

			scene.add( tile );
			
			return tile;
		};

		for(var i = 0; i < map.length; i++ ) {
			var type = map[i];
			var x = x0 + i % map_size * tile_size;
			var y = y0 + Math.floor(i / map_size) * tile_size;

			this.tiles.push(drawTile(this.scene, x, y, tile_size, type));
		}
	};

    Game.prototype.render = function() {
        var time = Date.now();
        this.frameTime = time - this.lastFrameTime;
        this.lastFrameTime = time;
        this.cumulatedFrameTime += this.frameTime;

        while(this.cumulatedFrameTime > this.options.controls.gameStepTime) {
			this.raycaster.setFromCamera( this.mouse, this.camera );
			var intersects = this.raycaster.intersectObjects( this.tiles );
			
			for(var i = 0; i < this.tiles.length; i++) {
				var tile = this.tiles[i];
				if(this.camera.position.distanceTo(tile.position) > 200) {
					tile.material.visible = false;
				} else {
					tile.material.visible = true;
				}
			}
			
			for ( var i = 0; i < this.tiles.length; i++ ) {
				this.tiles[i].material.color.set( this.tiles[i].initColor );
			}
			
			for ( var i = 0; i < intersects.length; i++ ) {
				if(intersects[i].object.name == 'Tile') {
					intersects[i].object.material.color.set( 0x0000ff );
				}
			}
			
			//this.step += this.options.controls.bouncingSpeed;
            //this.cube.position.x = this.cube.position.x + ( 10*(Math.cos(this.step)));
            //this.cube.position.y = this.cube.position.y + ( 10*Math.abs(Math.sin(this.step)));

            this.cumulatedFrameTime -= this.options.controls.gameStepTime;
        }

        this.renderer.render( this.scene, this.camera );

        if(this.options.stats !== undefined) {
            this.options.stats.update();
        }
    };

    Game.prototype.animate = function() {
        this.render();

        requestAnimationFrame( this.animate );
        this.camera_controls.update();
    };

    Game.prototype.onWindowResize = function() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize( window.innerWidth, window.innerHeight );

        this.render();
    };
})(window, document, THREE, _);